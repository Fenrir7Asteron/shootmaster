﻿using System;
using System.Collections.Generic;

namespace Code.Infrastructure.Services
{
  public class ServiceContainer
  {
    public readonly Dictionary<Type, IService> services;

    public ServiceContainer()
    {
      services = new Dictionary<Type, IService>();
    }

    public TService GetService<TService>() where TService : IService => 
      (TService) services[typeof(TService)];
  }
}