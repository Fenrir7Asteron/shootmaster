﻿namespace Code.Infrastructure.Services
{
  public class ServiceContext
  {
    // Class to be used only by Monobehaviours.
    // Other scripts must use explicit ServiceContainer instance.
    
    public readonly ServiceContainer ServiceContainer;
    
    private static ServiceContext _instance;

    public static void Initialize(ServiceContainer serviceContainer)
    {
      if (_instance == null)
      {
        _instance = new ServiceContext(serviceContainer);
      }
    }

    public static ServiceContext Instance() => _instance;

    private ServiceContext(ServiceContainer serviceContainer)
    {
      ServiceContainer = serviceContainer;
    }
  }
}