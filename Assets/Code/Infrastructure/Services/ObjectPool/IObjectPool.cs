﻿using UnityEngine;

namespace Code.Infrastructure.Services.ObjectPool
{
    public interface IObjectPool : IService
    {
        void CreatePool<TPooled>(GameObject objectToPool, int poolSize) where TPooled : IPooled;
        GameObject GetPooledObject<TPooled>() where TPooled : IPooled;
    }
}