﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Code.Infrastructure.Services.ObjectPool
{
    class ObjectPool : IObjectPool
    {
        private Dictionary<Type, List<GameObject>> pools;

        public ObjectPool()
        {
            pools = new Dictionary<Type, List<GameObject>>();
        }
        
        public void CreatePool<TPooled>(GameObject objectToPool, int poolSize) where TPooled : IPooled
        {
            Type poolType = typeof(TPooled);
            pools[poolType] = new List<GameObject>();
            
            GameObject tmp;
            for (int i = 0; i < poolSize; ++i)
            {
                tmp = Object.Instantiate(objectToPool);
                tmp.SetActive(false);
                pools[poolType].Add(tmp);
            }
        }

        public GameObject GetPooledObject<TPooled>() where TPooled : IPooled
        {
            Type poolType = typeof(TPooled);
            List<GameObject> pool = pools[poolType];

            foreach (GameObject poolObject in pool)
            {
                if (!poolObject.activeInHierarchy)
                {
                    return poolObject;
                }
            }

            return null;
        }
    }
}