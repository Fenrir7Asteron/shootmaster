﻿using Code.Infrastructure.StateMachine.States;

namespace Code.Infrastructure.Services.LevelLoader
{
    public interface ILevelLoader : IService
    {
        void TryLoadLevel(LevelPayload levelPayload);
    }
}