﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Code.Infrastructure.Services.LevelLoader
{
    public class SceneLoader : MonoBehaviour, IService
    {
        [Header("Canvas")] 
        public CanvasGroup canvasGroup;
        public float fadeDuration = 0.75f;
        
        public void LoadScene(string sceneNameToLoad, Action onLoaded) => 
            StartCoroutine(Load(sceneNameToLoad, onLoaded));

        private void Start() => 
            DontDestroyOnLoad(gameObject);

        private IEnumerator Load(string sceneNameToLoad, Action onLoaded)
        {
            StartCoroutine(ShowFade());

            AsyncOperation sceneLoader = SceneManager.LoadSceneAsync(sceneNameToLoad, LoadSceneMode.Single);

            yield return StartCoroutine(LoadGameAsync(sceneLoader));

            StartCoroutine(HideFade());

            onLoaded?.Invoke();
        }

        private IEnumerator ShowFade()
        {
            yield return canvasGroup.DOFade(1.0f, fadeDuration).WaitForCompletion();
        }
        
        private IEnumerator HideFade()
        {
            yield return canvasGroup.DOFade(0.0f, fadeDuration).WaitForCompletion();
        }

        private IEnumerator LoadGameAsync(AsyncOperation sceneLoader)
        {
            // Loading smoothed out using minimum step for the progress bar.
            while (!sceneLoader.isDone)
            {
                yield return null;
            }
        }
    }
}
