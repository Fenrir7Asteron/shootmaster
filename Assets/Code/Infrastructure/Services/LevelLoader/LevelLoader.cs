﻿using Code.Infrastructure.StateMachine;
using Code.Infrastructure.StateMachine.States;

namespace Code.Infrastructure.Services.LevelLoader
{
    public class LevelLoader : ILevelLoader
    {
        private readonly GameStateMachine _stateMachine;

        public LevelLoader(GameStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public void TryLoadLevel(LevelPayload levelPayload)
        {
            if (_stateMachine.CurrentState.GetType() == typeof(GameLoopState))
            {
                _stateMachine.Enter<SceneLoadingState, LevelPayload>(levelPayload);
            }
        }
    }
}