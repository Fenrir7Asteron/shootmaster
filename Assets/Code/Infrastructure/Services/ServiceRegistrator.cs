﻿using System;

namespace Code.Infrastructure.Services
{
  public class ServiceRegistrator
  {
    private readonly ServiceContainer _serviceContainer;
    
    public ServiceRegistrator(ServiceContainer serviceContainer)
    {
      _serviceContainer = serviceContainer;
    }

    public void RegisterSingle<TService>(TService implementation) where TService : IService
    {
      Type type = typeof(TService);
      
      if (!_serviceContainer.services.ContainsKey(type))
      {
        _serviceContainer.services.Add(type, implementation);
      }
    }

    public void UnregisterSingle<TService>() where TService : IService =>
      _serviceContainer.services.Remove(typeof(TService));
  }
}