﻿using UnityEngine;

namespace Code.Infrastructure.AssetManagement
{
    public class AssetProvider : IAssetProvider
    {
        public TGameObject Instantiate<TGameObject>(string path) where TGameObject : Object
        {
            TGameObject gameObject = Resources.Load<TGameObject>(path);
            return Object.Instantiate(gameObject);
        }

        public TGameObject Instantiate<TGameObject>(string path, Vector3 spawnPoint) where TGameObject : Object
        {
            TGameObject gameObject = Resources.Load<TGameObject>(path);
            return Object.Instantiate(gameObject, spawnPoint, Quaternion.identity);
        }

        public TGameObject Instantiate<TGameObject>(string path, Transform spawnTransform) where TGameObject : Object
        {
            TGameObject gameObject = Resources.Load<TGameObject>(path);
            return Object.Instantiate(gameObject, spawnTransform.position, spawnTransform.rotation);
        }

        public TGameObject Instantiate<TGameObject>(string path, GameObject parent) where TGameObject : Object
        {
            TGameObject gameObject = Resources.Load<TGameObject>(path);
            return Object.Instantiate(gameObject, parent.transform);
        }

        public TGameObject Instantiate<TGameObject>(string path, Transform spawnTransform,
            GameObject parent) where TGameObject : Object
        {
            TGameObject gameObject = Resources.Load<TGameObject>(path);
            return Object.Instantiate(gameObject, spawnTransform.position, spawnTransform.rotation, parent.transform);
        }

        public TGameObject Load<TGameObject>(string path) where TGameObject : Object
        {
            return Resources.Load<TGameObject>(path);
        }

        public TGameObject[] LoadAllFolder<TGameObject>(string directoryPath) where TGameObject : Object
        {
            return Resources.LoadAll<TGameObject>(directoryPath);
        }
    }
}