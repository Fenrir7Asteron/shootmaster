﻿namespace Code.Infrastructure.AssetManagement
{
    public static class AssetPaths
    {
        public const string PlayerPath = "Prefabs/Player/Player";
        public const string MainCameraPath = "Prefabs/Camera/MainCamera";
        public const string VirtualCameraPath = "Prefabs/Camera/VirtualCamera3rdPerson";
        public const string ProjectilePath = "Prefabs/Projectiles/Projectile";
        public const string ProjectilePoolSizePath = "Data/LevelParameters/ProjectilePoolSize";
    }
}