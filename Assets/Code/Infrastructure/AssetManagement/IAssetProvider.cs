﻿using Code.Infrastructure.Services;
using UnityEngine;

namespace Code.Infrastructure.AssetManagement
{
  public interface IAssetProvider : IService
  {
    TGameObject Instantiate<TGameObject>(string path) where TGameObject : Object;
    TGameObject Instantiate<TGameObject>(string path, Vector3 spawnPoint) where TGameObject : Object;
    TGameObject Instantiate<TGameObject>(string path, Transform spawnTransform) where TGameObject : Object;
    TGameObject Instantiate<TGameObject>(string path, GameObject parent) where TGameObject : Object;
    TGameObject Instantiate<TGameObject>(string path, Transform spawnTransform, GameObject parent) where TGameObject : Object;
    TGameObject Load<TGameObject>(string path)  where TGameObject : Object;
    TGameObject[] LoadAllFolder<TGameObject>(string directoryPath) where TGameObject : Object;
  }
}