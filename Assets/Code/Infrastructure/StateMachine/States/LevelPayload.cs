﻿using UnityEngine;

namespace Code.Infrastructure.StateMachine.States
{
    [CreateAssetMenu(menuName = "Payload/LevelParameters")]
    public class LevelPayload : ScriptableObject
    {
        public string sceneName;
    }
}