﻿using Code.Gameplay.Levels;
using Code.Gameplay.Player;
using Code.Infrastructure.AssetManagement;
using Code.Infrastructure.Factory;
using Code.Infrastructure.Services;
using Code.Infrastructure.Services.LevelLoader;
using Code.Infrastructure.Services.ObjectPool;
using Object = UnityEngine.Object;

namespace Code.Infrastructure.StateMachine.States
{
    public class SceneLoadingState : IPayloadedState<LevelPayload>
    {
        private readonly GameStateMachine _stateMachine;
        private readonly SceneLoader sceneLoader;
        private readonly ServiceContainer _serviceContainer;
        private IGameFactory _gameFactory;
        private LevelPayload _levelPayload;

        public SceneLoadingState(GameStateMachine stateMachine, SceneLoader sceneLoader,
            ServiceContainer serviceContainer)
        {
            _stateMachine = stateMachine;
            this.sceneLoader = sceneLoader;
            _serviceContainer = serviceContainer;
        }

        public void Enter(LevelPayload levelPayload)
        {
            _levelPayload = levelPayload;
            sceneLoader.LoadScene(levelPayload.sceneName, OnLoaded);
        }

        public void Exit()
        {
            
        }

        private void OnLoaded()
        {
            CreateSceneObjects();
            
            _stateMachine.Enter<GameLoopState>();
        }

        private void CreateSceneObjects()
        {
            _gameFactory = _serviceContainer.GetService<IGameFactory>();
            IObjectPool objectPool = _serviceContainer.GetService<IObjectPool>();

            LevelLogic levelLogic = Object.FindObjectOfType<LevelLogic>();

            Player player = _gameFactory.CreatePlayer(levelLogic.playerStartTransform);
            _gameFactory.CreateMainCamera();
            _gameFactory.CreateVirtualCamera(player.cameraFollow, player.cameraLookAt, AssetPaths.VirtualCameraPath);

            _gameFactory.CreateProjectilePool(objectPool);
        }
    }
}