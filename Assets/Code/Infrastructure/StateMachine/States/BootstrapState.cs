﻿using Code.Common;
using Code.Gameplay.Input;
using Code.Gameplay.Levels;
using Code.Infrastructure.AssetManagement;
using Code.Infrastructure.Factory;
using Code.Infrastructure.Services;
using Code.Infrastructure.Services.LevelLoader;
using Code.Infrastructure.Services.ObjectPool;

namespace Code.Infrastructure.StateMachine.States
{
    public class BootstrapState : IState
    {
        private ICoroutineRunner _coroutineRunner;
        
        private readonly GameStateMachine _stateMachine;
        private readonly SceneLoader _sceneLoader;
        private readonly ServiceRegistrator _serviceRegistrator;
        private readonly ServiceContainer _serviceContainer;
        private readonly LevelPayload _firstLevelPayload;

        public BootstrapState(GameStateMachine stateMachine, SceneLoader sceneLoader,
            ServiceRegistrator serviceRegistrator, ServiceContainer serviceContainer, ICoroutineRunner coroutineRunner,
            LevelPayload firstLevelPayload)
        {
            _stateMachine = stateMachine;
            _sceneLoader = sceneLoader;
            _serviceRegistrator = serviceRegistrator;
            _serviceContainer = serviceContainer;
            _coroutineRunner = coroutineRunner;
            _firstLevelPayload = firstLevelPayload;

            RegisterServices();
        }
        
        public void Enter() => 
            _stateMachine.Enter<SceneLoadingState, LevelPayload>(_firstLevelPayload);

        public void Exit()
        {
            
        }

        private void RegisterServices()
        {
            IAssetProvider assetProvider = new AssetProvider();
            IGameFactory gameFactory = new GameFactory(assetProvider);
            ILevelLoader levelLoader = new LevelLoader(_stateMachine);
            IInputService inputService = new TouchInputService();
            IObjectPool objectPool = new ObjectPool();
            
            _serviceRegistrator.RegisterSingle<IAssetProvider>(assetProvider);
            _serviceRegistrator.RegisterSingle<IGameFactory>(gameFactory);
            _serviceRegistrator.RegisterSingle<ILevelLoader>(levelLoader);
            _serviceRegistrator.RegisterSingle<IInputService>(inputService);
            _serviceRegistrator.RegisterSingle<IObjectPool>(objectPool);
        }
    }
}