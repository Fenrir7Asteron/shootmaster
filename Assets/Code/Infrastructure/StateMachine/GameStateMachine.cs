﻿using System;
using System.Collections.Generic;
using Code.Gameplay.Levels;
using Code.Infrastructure.Services;
using Code.Infrastructure.Services.LevelLoader;
using Code.Infrastructure.StateMachine.States;

namespace Code.Infrastructure.StateMachine
{
    public class GameStateMachine
    {
        private Dictionary<Type, IExitableState> _states;
        private IExitableState currentState;

        public IExitableState CurrentState
        {
            get => currentState;
            private set => currentState = value;
        }

        public GameStateMachine(SceneLoader sceneLoader, ServiceRegistrator serviceRegistrator,
            ServiceContainer serviceContainer, LevelPayload firstLevelPayload, ICoroutineRunner coroutineRunner)
        {
            _states = new Dictionary<Type, IExitableState>()
            {
                [typeof(BootstrapState)] = new BootstrapState(this, sceneLoader, serviceRegistrator, serviceContainer,
                    coroutineRunner, firstLevelPayload),
                [typeof(SceneLoadingState)] = new SceneLoadingState(this, sceneLoader, serviceContainer),
                [typeof(GameLoopState)] = new GameLoopState(this),
            };
        }

        public void Enter<TState>() where TState : class, IState
        {
            IState state = ChangeState<TState>();
            state.Enter();
        }

        public void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload>
        {
            IPayloadedState<TPayload> state = ChangeState<TState>();
            state.Enter(payload);
        }

        private TState ChangeState<TState>() where TState : IExitableState
        {
            CurrentState?.Exit();
      
            TState state = GetState<TState>();
            CurrentState = state;
      
            return state;
        }

        private TState GetState<TState>() where TState : IExitableState => 
            (TState) _states[typeof(TState)];
    }
}