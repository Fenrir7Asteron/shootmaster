﻿using Cinemachine;
using Code.Gameplay.Player;
using Code.Infrastructure.Services;
using Code.Infrastructure.Services.ObjectPool;
using UnityEngine;

namespace Code.Infrastructure.Factory
{
  public interface IGameFactory : IService
  {
    Player CreatePlayer(Transform spawnPoint);
    Camera CreateMainCamera();
    CinemachineVirtualCamera CreateVirtualCamera(Transform follow, Transform lookAt,
      string path);
    GameObject CreateEmptyObject(string objectName, Vector3 spawnPosition = default);
    TGameObject CreateObject<TGameObject>(TGameObject prefab, Vector3 spawnPosition = default,
      Quaternion spawnRotation = default, Transform parent = null) where TGameObject : Object;

    void CreateProjectilePool(IObjectPool objectPool);
  }
}