﻿using Cinemachine;
using Code.Common.GameEvents;
using Code.Common.ScriptableObjects.NumberVariables;
using Code.Gameplay.Player;
using Code.Gameplay.Weapons;
using Code.Infrastructure.AssetManagement;
using Code.Infrastructure.Services.ObjectPool;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace Code.Infrastructure.Factory
{
    public class GameFactory : IGameFactory
    {
        private readonly IAssetProvider _assets;

        public GameFactory(IAssetProvider assets)
        {
            _assets = assets;
        }

        public Player CreatePlayer(Transform spawnPoint) => 
            _assets.Instantiate<Player>(AssetPaths.PlayerPath, spawnPoint.transform);

        public Camera CreateMainCamera() => 
            _assets.Instantiate<Camera>(AssetPaths.MainCameraPath);

        public CinemachineVirtualCamera CreateVirtualCamera(Transform follow,
            Transform lookAt,
            string path)
        {
            CinemachineVirtualCamera vCam = _assets.Instantiate<CinemachineVirtualCamera>(path);
            vCam.Follow = follow;
            vCam.LookAt = lookAt;

            return vCam;
        }

        public GameObject CreateEmptyObject(string objectName, Vector3 spawnPosition)
        {
            GameObject emptyGameObject = new GameObject(objectName);
            emptyGameObject.transform.position = spawnPosition;
            return emptyGameObject;
        }

        public TGameObject CreateObject<TGameObject>(TGameObject prefab, Vector3 spawnPosition = default,
            Quaternion spawnRotation = default, Transform parent = null) where TGameObject : Object
        {
            return Object.Instantiate(prefab, spawnPosition, spawnRotation, parent);
        }

        public void CreateProjectilePool(IObjectPool objectPool)
        {
            GameObject projectile = _assets.Load<GameObject>(AssetPaths.ProjectilePath);
            IntVariable poolSize = _assets.Load<IntVariable>(AssetPaths.ProjectilePoolSizePath);
            
            objectPool.CreatePool<Projectile>(projectile, poolSize.value);
        }
    }
}