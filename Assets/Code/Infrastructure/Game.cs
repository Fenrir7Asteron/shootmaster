﻿using Code.Gameplay.Levels;
using Code.Infrastructure.Services;
using Code.Infrastructure.Services.LevelLoader;
using Code.Infrastructure.StateMachine;
using Code.Infrastructure.StateMachine.States;

namespace Code.Infrastructure
{
    public class Game
    {
        public GameStateMachine StateMachine;

        public Game(SceneLoader sceneLoader, ServiceRegistrator serviceRegistrator,
            ServiceContainer serviceContainer, LevelPayload firstLevel, ICoroutineRunner coroutineRunner)
        {
            StateMachine = new GameStateMachine(sceneLoader, serviceRegistrator, serviceContainer, firstLevel,
                coroutineRunner);
        }
    }
}