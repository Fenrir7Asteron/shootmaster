﻿using Code.Gameplay.Levels;
using Code.Infrastructure.Services;
using Code.Infrastructure.Services.LevelLoader;
using Code.Infrastructure.StateMachine.States;
using UnityEngine;

namespace Code.Infrastructure
{
    public class GameBootstrapper : MonoBehaviour, ICoroutineRunner
    {
        public SceneLoader sceneLoader;
        public LevelPayload firstLevel; 
        
        private Game _game;
        private ServiceContainer _serviceContainer;

        private void Awake()
        {
            Application.targetFrameRate = 60;
        }

        private void Start()
        {
            ServiceContainer serviceContainer = new ServiceContainer();
            
            ServiceContext.Initialize(serviceContainer);
            
            ServiceRegistrator serviceRegistrator = new ServiceRegistrator(serviceContainer);
            
            _game = new Game(sceneLoader, serviceRegistrator, serviceContainer, firstLevel, this);
            _game.StateMachine.Enter<BootstrapState>();
            
            DontDestroyOnLoad(gameObject);
        }
    }
}
