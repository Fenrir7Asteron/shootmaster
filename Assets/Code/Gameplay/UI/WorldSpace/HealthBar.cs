﻿using Code.Gameplay.Enemies;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Gameplay.UI.WorldSpace
{
    public class HealthBar : MonoBehaviour
    {
        [Header("Health")] 
        public Health health;
        
        [Header("UI")]
        public Slider healthBar;
        public GameObject healthBarVisual;

        private void Awake()
        {
            healthBar.maxValue = health.maxHp.Value;
            healthBar.value = healthBar.maxValue;

            health.OnHealthChanged += () => healthBar.value = health.CurrentHp;
            health.OnZeroHealth += () => enabled = false;
        }
    
        private void OnEnable()
        {
            healthBarVisual.SetActive(true);
        }

        private void OnDisable()
        {
            healthBarVisual.SetActive(false);
        }
    }
}
