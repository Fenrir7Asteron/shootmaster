﻿using UnityEngine;

namespace Code.Gameplay.Locomotion
{
    public class LinearMovement : Movement
    {
        public override void MoveAgent(GameObject agent, float speed)
        {
            agent.transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);
        }
    }
}
