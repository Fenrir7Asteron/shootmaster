﻿using Code.Common.ScriptableObjects;
using UnityEngine;
using UnityEngine.AI;

namespace Code.Gameplay.Locomotion
{
    public class WaypointMovement : MonoBehaviour
    {
        [Header("Waypoints")]
        public RuntimeSetWaypoints waypointSet;
        public float waypointRadius = 0.3f;
        
        [Header("NavMesh")]
        public NavMeshAgent agent;
        
        private int _currentWP;

        private void Start()
        {
            var waypoint = waypointSet.items[_currentWP];

            if (waypointSet != null && waypointSet.items != null && waypoint != null)
            {
                agent.SetDestination(waypoint.transform.position);
            }
        }

        private void Update()
        {
            if (waypointSet == null || waypointSet.items == null || waypointSet.items.Count <= _currentWP)
            {
                return;
            }

            var waypoint = waypointSet.items[_currentWP];
            agent.SetDestination(waypoint.transform.position);

            var isNearWaypoint = Vector3.Distance(waypoint.transform.position, agent.transform.position) < 
                                 waypointRadius;
        
            agent.isStopped = isNearWaypoint;

            if (isNearWaypoint)
            {
                if (waypoint.IsWaypointClear())
                {
                    ++_currentWP;
                }
            }
        }
    }
}
