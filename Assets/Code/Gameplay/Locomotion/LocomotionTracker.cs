﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace Code.Gameplay.Locomotion
{
    public class LocomotionTracker : MonoBehaviour
    {
        public NavMeshAgent navMeshAgent;
        public Transform visual;
        public Animator animator;
        
        private static readonly int RunXSpeed = Animator.StringToHash("RunXSpeed");
        private static readonly int RunZSpeed = Animator.StringToHash("RunZSpeed");
        private Quaternion _startRotationOffset;

        private void Start()
        {
            _startRotationOffset = Quaternion.Inverse(visual.rotation);
        }

        private void Update()
        {
            Vector3 relativeVelocity = FindRelativeVelocity();

            animator.SetFloat(RunXSpeed, relativeVelocity.x);
            animator.SetFloat(RunZSpeed, relativeVelocity.z);
        }

        private Vector3 FindRelativeVelocity()
        {
            Vector3 velocity = navMeshAgent.velocity;
            velocity.y = 0;
            velocity.Normalize();

            Vector3 lookDirection = visual.forward;
            lookDirection.y = 0;
            float angle = Vector3.SignedAngle(lookDirection, velocity, Vector3.up);

            Vector3 relativeVelocity = _startRotationOffset
                                       * Quaternion.Euler(0.0f, angle, 0.0f)
                                       * velocity;
            return relativeVelocity;
        }
    }
}
