﻿using UnityEngine;

namespace Code.Gameplay.Locomotion
{
    public abstract class Movement
    {
        public abstract void MoveAgent(GameObject agent, float speed);
    }
}
