﻿using Code.Infrastructure;
using Code.Infrastructure.Services;
using Code.Infrastructure.Services.LevelLoader;
using Code.Infrastructure.StateMachine.States;
using UnityEngine;

namespace Code.Gameplay.Levels
{
    public class Finish : MonoBehaviour
    {
        public LevelPayload nextLevel;
        
        private ILevelLoader _sceneLoader;

        private void Awake()
        {
            _sceneLoader = ServiceContext.Instance().ServiceContainer.GetService<ILevelLoader>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                _sceneLoader.TryLoadLevel(nextLevel);
            }
        }
    }
}
