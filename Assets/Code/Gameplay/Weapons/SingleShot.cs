﻿using System;
using Code.Gameplay.Locomotion;
using Code.Infrastructure.Services;
using Code.Infrastructure.Services.ObjectPool;
using UnityEngine;

namespace Code.Gameplay.Weapons
{
    public class SingleShot : Weapon
    {
        private IObjectPool _projectilePool;

        public override void Shoot()
        {
            foreach (var spawnPlace in projectileSpawnPlaces)
            {
                GameObject projectileInstance = _projectilePool.GetPooledObject<Projectile>();
                if (!projectileInstance)
                {
                    return;
                }

                projectileInstance.transform.position = spawnPlace.position;
                projectileInstance.transform.rotation = spawnPlace.rotation;

                if (projectileInstance.TryGetComponent(out Projectile projectileComponent))
                {
                    projectileComponent.SetWeaponData(weaponData);
                    projectileComponent.SetProjectileData(weaponData.projectileData);
                    projectileComponent.SetMovementType(new LinearMovement());
                }

                projectileInstance.SetActive(true);
            }
        }

        private void Start()
        {
            _projectilePool = ServiceContext.Instance().ServiceContainer.GetService<IObjectPool>();
        }
    }
}
