﻿using Code.Common.ScriptableObjects;
using Code.Gameplay.Enemies;
using Code.Gameplay.Locomotion;
using Code.Infrastructure.Services.ObjectPool;
using UnityEngine;

namespace Code.Gameplay.Weapons
{
    public class Projectile : MonoBehaviour, IPooled
    {
        private ProjectileData _projectileData;
        private WeaponData _originWeaponData;
        private Movement _movement;
        private Vector3 _startPosition;

        public void SetWeaponData(WeaponData originWeaponData)
        {
            this._originWeaponData = originWeaponData;
        }

        public void SetProjectileData(ProjectileData projectileData)
        {
            this._projectileData = projectileData;
        }

        public void SetMovementType(Movement movement)
        {
            this._movement = movement;
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.TryGetComponent(out Health health))
            {
                if (_originWeaponData != null && _originWeaponData.damage != null)
                {
                    health.DamageHp(_originWeaponData.damage.Value);
                }
            }
            
            ExplodeProjectile();
        }

        private void FixedUpdate()
        {
            _movement?.MoveAgent(gameObject, _projectileData.speed.Value);
            if (Vector3.Distance(transform.position, _startPosition) > _projectileData.lifeDistance.Value)
            {
                ExplodeProjectile();
            }
        }

        private void ExplodeProjectile()
        {
            gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            _startPosition = transform.position;
        }
    }
}
