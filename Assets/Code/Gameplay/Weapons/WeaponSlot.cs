﻿using UnityEngine;

namespace Code.Gameplay.Weapons
{
    public class WeaponSlot : MonoBehaviour
    {
        public delegate void WeaponChanged();

        public event WeaponChanged OnWeaponChanged;
        
        public GameObject weaponSlot;
        public GameObject weaponToEquip;

        private GameObject _equippedWeapon;

        public void EquipWeapon(GameObject weapon)
        {
            if (_equippedWeapon == null)
            {
                _equippedWeapon = Instantiate(weapon, weaponSlot.transform);
                OnWeaponChanged?.Invoke();
            }
        }

        public void RemoveWeapon()
        {
            if (_equippedWeapon != null)
            {
                Destroy(_equippedWeapon);
                OnWeaponChanged?.Invoke();
            }
        }

        public Weapon GetEquippedWeapon()
        {
            if (_equippedWeapon != null)
            {
                if (_equippedWeapon.TryGetComponent(out Weapon weaponScript))
                {
                    return weaponScript;
                }
            }

            return null;
        }

        private void OnEnable()
        {
            EquipWeapon(weaponToEquip);
        }

        private void OnDisable()
        {
            RemoveWeapon();
        }
    }
}
