﻿using Code.Common.ScriptableObjects;
using UnityEngine;

namespace Code.Gameplay.Weapons
{
    public abstract class Weapon : MonoBehaviour
    {
        public Transform[] projectileSpawnPlaces;
        public GameObject projectile;
        public WeaponData weaponData;

        public abstract void Shoot();
    }
}
