﻿using UnityEngine;

namespace Code.Gameplay.Player
{
    public class Player : MonoBehaviour
    {
        [Header("Camera")]
        public Transform cameraFollow;
        public Transform cameraLookAt;
    }
}
