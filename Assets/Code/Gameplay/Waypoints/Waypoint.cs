﻿using Code.Common.ScriptableObjects;
using Code.Common.ScriptableObjects.NumberVariables;
using UnityEngine;

namespace Code.Gameplay.Waypoints
{
    public class Waypoint : MonoBehaviour
    {
        public delegate void ReachedWaypoint();

        public event ReachedWaypoint OnReachedWaypoint;
    
        public IntReference positionalNumber;

        public bool IsWaypointClear()
        {
            if (enemiesInZone == null || enemiesInZone.items == null)
            {
                return true;
            }
            return enemiesInZone.items.Count == 0;
        }
    
        [SerializeField] private RuntimeSetWaypoints waypointSet;
        [SerializeField] private RuntimeSetEnemiesInZone enemiesInZone;
        private void OnEnable()
        {
            // If the number is not stated, set the last possible.
            if (positionalNumber.Value == -1)
            {
                if (waypointSet != null && waypointSet.items != null)
                {
                    positionalNumber.Value = waypointSet.items.Count;
                }
            }
            waypointSet.Add(positionalNumber.Value, this);
        }

        private void OnDisable()
        {
            waypointSet.Remove(positionalNumber.Value);
        }
    }
}
