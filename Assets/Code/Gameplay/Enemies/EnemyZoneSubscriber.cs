﻿using Code.Common.ScriptableObjects;
using UnityEngine;

namespace Code.Gameplay.Enemies
{
    public class EnemyZoneSubscriber : MonoBehaviour
    {
        [Header("Health")] 
        public Health health;
        
        [Header("Runtime sets")]
        public RuntimeSetEnemiesInZone enemiesInZone;

        private void Awake()
        {
            health.OnZeroHealth += () => enabled = false;
        }

        private void OnEnable()
        {
            enemiesInZone.Add(this);
        }

        private void OnDisable()
        {
            enemiesInZone.Remove(this);
        }
    }
}
