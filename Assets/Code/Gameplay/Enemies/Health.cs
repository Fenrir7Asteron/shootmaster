﻿using Code.Common.ScriptableObjects.NumberVariables;
using UnityEngine;

namespace Code.Gameplay.Enemies
{
    public class Health : MonoBehaviour
    {
        public delegate void HealthChanged();
        public delegate void ZeroHealth();
    
        public event HealthChanged OnHealthChanged;
        public event ZeroHealth OnZeroHealth;
    
        public IntReference maxHp;
        public int CurrentHp { get; private set; }

        public void DamageHp(int hpIncrement)
        {
            CurrentHp -= hpIncrement;

            if (CurrentHp <= 0)
            {
                OnZeroHealth?.Invoke();
                enabled = false;
            }
        
            OnHealthChanged?.Invoke();
        }
        private void Start()
        {
            CurrentHp = maxHp.Value;
            OnHealthChanged?.Invoke();
        }
    }
}
