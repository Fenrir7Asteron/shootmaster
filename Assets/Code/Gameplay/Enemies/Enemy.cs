﻿using UnityEngine;

namespace Code.Gameplay.Enemies
{
    public class Enemy : MonoBehaviour
    {
        [Header("Health")] 
        public Health health;
        
        [Header("Animations")]
        public Animator animator;

        [Header("Colliders")]
        public Collider collider;
        
        private static readonly int Dead = Animator.StringToHash("Dead");

        private void Awake()
        {
            health.OnZeroHealth += Die;
        }

        private void Die()
        {
            animator.SetTrigger(Dead);
            collider.enabled = false;
            enabled = false;
        }
    }
}
