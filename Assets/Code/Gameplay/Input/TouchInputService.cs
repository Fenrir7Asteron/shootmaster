﻿using UnityEngine;

namespace Code.Gameplay.Input
{
    public class TouchInputService : IInputService
    {
        private const string AttackButton = "Fire1";
        public bool IsAiming() => 
            UnityEngine.Input.GetButton(AttackButton);

        public bool IsShooting() => 
            UnityEngine.Input.GetMouseButtonUp(0);

        public Vector3 WorldSpaceTouch(Vector3 aimingPlaneCenter, Camera mainCamera)
        {
            Plane aimingPlane = new Plane(Vector3.up, aimingPlaneCenter);
            Ray touchRay = mainCamera.ScreenPointToRay(UnityEngine.Input.mousePosition);
            
            return aimingPlane.Raycast(touchRay, out float intersectionDistance) ?
                touchRay.GetPoint(intersectionDistance) : Vector3.negativeInfinity;
        }
    }
}