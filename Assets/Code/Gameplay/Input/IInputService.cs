﻿using Code.Infrastructure.Services;
using UnityEngine;

namespace Code.Gameplay.Input
{
    public interface IInputService : IService
    {
        bool IsAiming();
        bool IsShooting();
        Vector3 WorldSpaceTouch(Vector3 aimingPlaneCenter, Camera mainCamera);
    }
}