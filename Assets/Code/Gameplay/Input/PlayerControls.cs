﻿using Code.Common;
using Code.Gameplay.Weapons;
using Code.Infrastructure.Services;
using UnityEngine;

namespace Code.Gameplay.Input
{
    public class PlayerControls : MonoBehaviour
    {
        [Header("Weapon")] 
        public WeaponSlot weaponSlot;
        
        [Header("Aiming parameters")]
        public float maxYRotationDelta;

        private Camera _mainCamera;
        private Weapon _currentWeapon;
        private Transform _currentProjectileSpawnTransform;
        private Vector3 _startRotation;
        private IInputService _inputService;
        private Quaternion _targetRotation;
        private Quaternion _sourceRotation;
        private float _timePassed;

        private void Awake()
        {
            weaponSlot.OnWeaponChanged += () =>
            {
                _currentWeapon = weaponSlot.GetEquippedWeapon();
                if (_currentWeapon.projectileSpawnPlaces != null && _currentWeapon.projectileSpawnPlaces.Length > 0)
                {
                    _currentProjectileSpawnTransform = _currentWeapon.projectileSpawnPlaces[0];
                }
            };
        }

        private void Start()
        {
            _mainCamera = Camera.main;
            
            Quaternion startRotation = transform.rotation;
            _startRotation = startRotation.eulerAngles;
            _sourceRotation = startRotation;
            _targetRotation = startRotation;
            
            _inputService = ServiceContext.Instance().ServiceContainer.GetService<IInputService>();
        }

        private void Update()
        {
            if (_inputService.IsAiming())
            {
                UpdateRotationTarget();
            }

            if (_inputService.IsShooting())
            {
                _currentWeapon.Shoot();
            }

            UpdateRotation();
        }

        private void UpdateRotation()
        {
            _timePassed += Time.deltaTime;
            transform.rotation = Quaternion.Lerp(_sourceRotation, _targetRotation, _timePassed * 10);
        }

        private void UpdateRotationTarget()
        {
            Vector3 target = _inputService.WorldSpaceTouch(_currentProjectileSpawnTransform.position, _mainCamera);
            Vector3 direction = target - transform.position;

            float gunBodyOffset = Vector3.Angle(transform.forward, _currentProjectileSpawnTransform.forward);

            float rotation = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg - gunBodyOffset;

            if (rotation < Constants.EPS)
            {
                rotation = rotation + 180.0f;
            }

            if (IsInsideRange(rotation, _startRotation.y - maxYRotationDelta,
                _startRotation.y + maxYRotationDelta))
            {
                _sourceRotation = transform.rotation;
                _targetRotation = Quaternion.Euler(0.0f, rotation, 0.0f);
                _timePassed = 0.0f;
            }
        }

        private bool IsInsideRange(float value, float min, float max) =>
            min + Constants.EPS < value && value + Constants.EPS < max;
    }
}