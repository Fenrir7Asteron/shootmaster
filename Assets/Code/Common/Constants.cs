﻿namespace Code.Common
{
    public static class Constants
    {
        public const float EPS = 0.01f;
        
        public const string SceneNameInitial = "Initial";
    }
}