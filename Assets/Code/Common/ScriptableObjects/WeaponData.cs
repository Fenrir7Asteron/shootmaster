﻿using Code.Common.ScriptableObjects.NumberVariables;
using UnityEngine;

namespace Code.Common.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Weapons/WeaponData", order = 53)]
    public class WeaponData : ScriptableObject
    {
        public IntReference damage;
        public ProjectileData projectileData;
    }
}
