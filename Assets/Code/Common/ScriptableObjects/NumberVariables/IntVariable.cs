﻿using UnityEngine;

namespace Code.Common.ScriptableObjects.NumberVariables
{
    [CreateAssetMenu(menuName = "Variables/Int", order = 55)]
    public class IntVariable : ScriptableObject
    {
        public int value;
    }
}