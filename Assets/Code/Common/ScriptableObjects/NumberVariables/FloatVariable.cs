﻿using UnityEngine;

namespace Code.Common.ScriptableObjects.NumberVariables
{
    [CreateAssetMenu(menuName = "Variables/Float", order = 56)]
    public class FloatVariable : ScriptableObject
    {
        public float value;
    }
}
