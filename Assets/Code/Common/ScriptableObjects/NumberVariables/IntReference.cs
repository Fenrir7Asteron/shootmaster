﻿using System;
using Code.Common.DrawIfAttribute;

namespace Code.Common.ScriptableObjects.NumberVariables
{
    [Serializable]
    public class IntReference
    {
        public UseChoice useChoice;
        [DrawIfAttribute.DrawIf("useChoice", UseChoice.UseConstant, ComparisonType.Equals)]
        public int constantValue;
        [DrawIfAttribute.DrawIf("useChoice", UseChoice.UseDataVariable, ComparisonType.Equals)]
        public IntVariable dataVariable;
        [DrawIfAttribute.DrawIf("useChoice", UseChoice.UseMemoryVariable, ComparisonType.Equals)]
        public int memoryVariable;

        public int Value
        {
            get
            {
                switch (useChoice)
                {
                    case UseChoice.UseConstant:
                        return constantValue;
                    case UseChoice.UseDataVariable:
                        return dataVariable.value;
                    case UseChoice.UseMemoryVariable:
                        return memoryVariable;
                    default:
                        return 0;
                }
            }
            set
            {
                switch (useChoice)
                {
                    case UseChoice.UseDataVariable:
                        dataVariable.value = value;
                        break;
                    case UseChoice.UseMemoryVariable:
                        memoryVariable = value;
                        break;
                }
            }
        }
    }
}
