﻿using System;
using Code.Common.DrawIfAttribute;

namespace Code.Common.ScriptableObjects.NumberVariables
{
    [Serializable]
    public class FloatReference
    {
        public UseChoice useChoice;
        [DrawIfAttribute.DrawIf("useChoice", UseChoice.UseConstant, ComparisonType.Equals)]
        public float constantValue;
        [DrawIfAttribute.DrawIf("useChoice", UseChoice.UseDataVariable, ComparisonType.Equals)]
        public FloatVariable dataVariable;
        [DrawIfAttribute.DrawIf("useChoice", UseChoice.UseMemoryVariable, ComparisonType.Equals)]
        public float memoryVariable;

        public float Value
        {
            get
            {
                switch (useChoice)
                {
                    case UseChoice.UseConstant:
                        return constantValue;
                    case UseChoice.UseDataVariable:
                        return dataVariable.value;
                    case UseChoice.UseMemoryVariable:
                        return memoryVariable;
                    default:
                        return 0;
                }
            }
            set
            {
                switch (useChoice)
                {
                    case UseChoice.UseDataVariable:
                        dataVariable.value = value;
                        break;
                    case UseChoice.UseMemoryVariable:
                        memoryVariable = value;
                        break;
                }
            }
        }
    }
}
