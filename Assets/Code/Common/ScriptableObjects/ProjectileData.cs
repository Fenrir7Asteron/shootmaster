﻿using Code.Common.ScriptableObjects.NumberVariables;
using UnityEngine;

namespace Code.Common.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Projectiles/ProjectileData", order = 54)]
    public class ProjectileData : ScriptableObject
    {
        public FloatReference speed;
        public FloatReference lifeDistance;
    }
}
