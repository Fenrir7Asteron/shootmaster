﻿using System.Collections.Generic;
using UnityEngine;

namespace Code.Common.ScriptableObjects.RuntimeSets
{
    public abstract class RuntimeSetOrdered<T> : ScriptableObject
    {
        public SortedDictionary<int, T> items = new SortedDictionary<int, T>();
        
        public void Add(int key, T value)
        {
            if (!items.ContainsKey(key))
            {
                items.Add(key, value);
            }
        }
        
        public void Remove(int key)
        {
            if (items.ContainsKey(key))
            {
                items.Remove(key);
            }
        }
    }
}