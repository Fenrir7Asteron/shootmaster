﻿using Code.Common.ScriptableObjects.RuntimeSets;
using Code.Gameplay.Enemies;
using UnityEngine;

namespace Code.Common.ScriptableObjects
{
    [CreateAssetMenu(menuName = "RuntimeSets/Unordered/EnemiesInZone", order = 52)]
    public class RuntimeSetEnemiesInZone : RuntimeSetUnordered<EnemyZoneSubscriber>
    {

    }
}
