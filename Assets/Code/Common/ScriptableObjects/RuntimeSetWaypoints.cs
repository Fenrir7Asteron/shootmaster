﻿using Code.Common.ScriptableObjects.RuntimeSets;
using Code.Gameplay.Waypoints;
using UnityEngine;

namespace Code.Common.ScriptableObjects
{
    [CreateAssetMenu(menuName = "RuntimeSets/Ordered/Waypoints", order = 51)]
    public class RuntimeSetWaypoints : RuntimeSetOrdered<Waypoint>
    {

    }
}
