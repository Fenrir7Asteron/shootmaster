﻿using UnityEditor;

namespace Code.Common.DrawIfAttribute.Editor
{
    public static class SerializedPropertyExtentions
    {
	    public static T GetValue<T>(this SerializedProperty property)
        {
            return (T) ReflectionUtil.GetTargetObjectOfProperty(property, property.propertyPath);
        }
    }
}