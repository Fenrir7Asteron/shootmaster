﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

namespace Code.Common.DrawIfAttribute.Editor
{
    [CustomPropertyDrawer(typeof(Code.Common.DrawIfAttribute.DrawIfAttribute))]
    public class DrawIfPropertyDrawer : PropertyDrawer
    {
        // Reference to the attribute on the property.
        Code.Common.DrawIfAttribute.DrawIfAttribute drawIf;

        // Field that is being compared.
        SerializedProperty comparedField;

        // Height of the property.
        private float propertyHeight;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (!ShowMe(property) && drawIf.DisablingType == DisablingType.DontDraw) {
                return -EditorGUIUtility.standardVerticalSpacing;
            }
            else
            {
                if(property.propertyType == SerializedPropertyType.Generic)
                {
                    int numChildren = 0;
                    float totalHeight = 0.0f;
 
                    IEnumerator children = property.GetEnumerator();
 
                    while (children.MoveNext())
                    {
                        SerializedProperty child = children.Current as SerializedProperty;
                 
                        GUIContent childLabel = new GUIContent(child.displayName);
 
                        totalHeight += EditorGUI.GetPropertyHeight(child, childLabel) + EditorGUIUtility.standardVerticalSpacing;              
                        numChildren ++;
                    }
 
                    // Remove extra space at end, (we only want spaces between items)
                    totalHeight -= EditorGUIUtility.standardVerticalSpacing;
 
                    return totalHeight;
                }
 
                return EditorGUI.GetPropertyHeight(property, label);
            }
        }
        
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // If the condition is met, simply draw the field.
            if (ShowMe(property))
            {
                // A Generic type means a custom class...
                if(property.propertyType == SerializedPropertyType.Generic)
                {
                    IEnumerator children = property.GetEnumerator();
 
                    Rect offsetPosition = position;
 
                    while (children.MoveNext())
                    {
                        SerializedProperty child = children.Current as SerializedProperty;
                 
                        GUIContent childLabel = new GUIContent(child.displayName);
 
                        float childHeight = EditorGUI.GetPropertyHeight(child, childLabel);
                        offsetPosition.height = childHeight;
 
                        EditorGUI.PropertyField(offsetPosition, child, childLabel);
                 
                        offsetPosition.y += childHeight + EditorGUIUtility.standardVerticalSpacing;
                    }
                }
                else
                {
                    EditorGUI.PropertyField(position, property, label);
                }
 
            } //...check if the disabling type is read only. If it is, draw it disabled
            else if (drawIf.DisablingType == DisablingType.ReadOnly)
            {
                GUI.enabled = false;
                EditorGUI.PropertyField(position, property, label);
                GUI.enabled = true;
            }
        }
        
        /// <summary>
        /// Errors default to showing the property.
        /// </summary>
        private bool ShowMe(SerializedProperty property)
        {
            drawIf = attribute as Code.Common.DrawIfAttribute.DrawIfAttribute;
            // Replace propertyname to the value from the parameter
            string path = property.propertyPath.Replace(".Array.data[", "[");
            string[] pathSplit = path.Split('.');
            pathSplit[pathSplit.Length - 1] = drawIf.ComparedPropertyName;

            path = string.Join(".", pathSplit);
            
            object obj = ReflectionUtil.GetTargetObjectOfProperty(property, path);
 
            if (obj == null)
            {
                Debug.LogError("Cannot find property with name: " + path);
                return true;
            }

            if (obj.GetType().IsSubclassOf(typeof(Enum)) 
                && drawIf.ComparedValue.GetType().IsSubclassOf(typeof(Enum)))
            {
                int x1 = (int) obj;
                int x2 = (int) drawIf.ComparedValue;

                return Compare(x1, x2);
            }

            if (obj is IComparable 
                && drawIf.ComparedValue is IComparable)
            {
                IComparable x1 = (IComparable) obj;
                IComparable x2 = (IComparable) drawIf.ComparedValue;

                return Compare(x1, x2);
            }

            return obj.Equals(drawIf.ComparedValue);
        }

        private bool Compare(IComparable x1, IComparable x2)
        {
            switch (drawIf.ComparisonType)
            {
                case ComparisonType.Equals:
                    return x1.CompareTo(x2) == 0;
                case ComparisonType.NotEqual:
                    return x1.CompareTo(x2) != 0;
                case ComparisonType.GreaterThan:
                    return x1.CompareTo(x2) > 0;
                case ComparisonType.SmallerThan:
                    return x1.CompareTo(x2) < 0;
                case ComparisonType.GreaterOrEqual:
                    return x1.CompareTo(x2) >= 0;
                case ComparisonType.SmallerOrEqual:
                    return x1.CompareTo(x2) <= 0;
                default:
                    return true;
            }
        }
    }
}
