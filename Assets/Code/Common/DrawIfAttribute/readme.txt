A tool to draw or to hide properties in the Inspector based on a state of some other sibling property.

The tool is taken from here: https://drive.google.com/drive/folders/0BwN2sNbqJn9mLXNqNnFWd2dUUFk

Forum link is here: https://forum.unity.com/threads/draw-a-field-only-if-a-condition-is-met.448855/

The version of the code in this project includes fixes for nested objects.