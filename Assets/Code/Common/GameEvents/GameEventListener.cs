﻿using UnityEngine;
using UnityEngine.Events;

namespace Code.Common.GameEvents
{
    public class GameEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        public GameEvent myEvent;
        
        [Tooltip("Response to invoke when Event is raised.")]
        public UnityEvent response;

        public void OnEventRaised() => response.Invoke();
        
        private void OnEnable() => myEvent.RegisterListener(this);

        private void OnDisable() => myEvent.UnregisterListener(this);
    }
}
